import { Component } from "@angular/core";
import { Sensor } from "./shared/models/sensor.model";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "my-second-project";
  newId = 11;
  newTitle = "";
  newStatus = "online";
  sensors: Sensor[] = [];
  onAddSensor() {
    console.log("!!!");
    if (this.newTitle != "") {
      this.sensors.push(
        new Sensor(this.newId, this.newTitle, this.newStatus == "online")
      );
      this.newId++;
      this.newTitle = "";
    }
  }
  onDeleteSensor(idx: number) {
    this.sensors.splice(idx, 1);
  }
  constructor() {
    for (let i = 0; i < 10; i++) {
      this.sensors.push(new Sensor(i, `Датчик_${i}`));
    }
    console.log(this.sensors);
  }
}
